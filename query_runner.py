import pandas as pd

from config import log, CSV_FILE_TO_READ, CSV_FILE_TO_WRITE, ACCOUNT_KEY


def query(address):
    """
    Query for get balance and timestamp of wallet
    :param address: address of wallet
    """
    return ('SELECT from_address, to_address, value, block_timestamp '
            'FROM `bigquery-public-data.ethereum_blockchain.token_transfers` '
            f'WHERE from_address = "{address}" '
            f'OR to_address = "{address}" '
            'AND '
            'token_address = "0xd4c435f5b09f855c3317c8524cb1f586e42795fa"')


def run_query(account_key, address):
    """
    Collect data of balance of wallet per day and save to .csv
    :param account_key: google account key in .json for auth
    :param address: address of wallet
    """
    try:
        df_sender = pd.read_gbq(query(address),
                                private_key=account_key,
                                dialect='standard')
        df_receiver = df_sender.copy()

        df_sender = (df_sender[df_sender['from_address'] == address].
                     drop(columns=['to_address']))
        df_receiver = (df_receiver[df_receiver['to_address'] == address].
                       drop(columns=['from_address']))

        df_sender['block_timestamp'] = df_sender['block_timestamp'].dt.date
        df_receiver['block_timestamp'] = df_receiver['block_timestamp'].dt.date

        df_sender['value'] = df_sender['value'].astype(float)
        df_receiver['value'] = df_receiver['value'].astype(float)
        df_sender['value'] = -df_sender['value']
        df = pd.concat([df_sender, df_receiver])
        df['value'] = df['value'].cumsum()

        balance_series = df.groupby("block_timestamp")['value'].sum()
        balance_series = pd.DataFrame(balance_series).T
        balance_series.index = pd.Index([address])
        return balance_series
    except Exception as exc:
        log.error(f'Error: {exc}')


if __name__ == '__main__':
    final_df = pd.DataFrame()
    i = 0
    for row in pd.read_csv(CSV_FILE_TO_READ).address:
        df = run_query(ACCOUNT_KEY, row)
        i += 1
        final_df = pd.concat([final_df, df],
                             axis=1,
                             sort=False)
        final_df = final_df.sort_index(axis=1)
        print(i)
        final_df.to_csv(CSV_FILE_TO_WRITE)
